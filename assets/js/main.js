let isMenuOpen = false;

function onMenu() {
    let menu = document.querySelector('.header .nav .items');
    if(isMenuOpen) { // Close
        menu.classList.remove('active');
        isMenuOpen = false;
    } else { // Open
        menu.classList.add('active');
        isMenuOpen = true;
    }
}

var acc = document.getElementsByClassName("accordion");
var i;
for (i = 0; i <acc.length;i++) {
    acc[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}